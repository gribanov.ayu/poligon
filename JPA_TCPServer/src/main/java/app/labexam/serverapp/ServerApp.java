package app.labexam.serverapp;

import app.labexam.serverapp.jpa.entity.Employee;
import app.labexam.serverapp.jpa.entity.EmployeeHistory;
import app.labexam.serverapp.jpa.services.EmployeeHistoryService;
import app.labexam.serverapp.jpa.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.time.LocalDateTime;

@SpringBootApplication
public class ServerApp {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	EmployeeHistoryService employeeHistoryService;

	public static void main(String[] args) {
		SpringApplication.run(ServerApp.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	private void initData(){
		Employee e = new Employee();
		e.setName("Fred");
		e.setLast_name("King");
		e.setLogin("admin");
		e.setPassword("1234");
		employeeService.createEmployee(e);

		EmployeeHistory eh = new EmployeeHistory();
		eh.setHire(LocalDateTime.now().minusDays(35).toLocalDate());
		eh.setDismiss(LocalDateTime.now().minusDays(15).toLocalDate());
		eh.setManagerId(1);
		eh.setPosition("CEO");
		eh.setEmployee(e);
		employeeHistoryService.createEmployeeHistory(eh);

		eh = new EmployeeHistory();
		eh.setHire(LocalDateTime.now().minusDays(50).toLocalDate());
		eh.setDismiss(LocalDateTime.now().minusDays(35).toLocalDate());
		eh.setManagerId(1);
		eh.setPosition("ABC");
		eh.setEmployee(e);
		employeeHistoryService.createEmployeeHistory(eh);

		e = new Employee();
		e.setName("Steve");
		e.setLast_name("Ballmer");
		e.setLogin("user");
		e.setPassword("5678");
		employeeService.createEmployee(e);

		eh = new EmployeeHistory();
		eh.setHire(LocalDateTime.now().minusDays(60).toLocalDate());
		eh.setDismiss(LocalDateTime.now().minusDays(25).toLocalDate());
		eh.setManagerId(2);
		eh.setPosition("Boss");
		eh.setEmployee(e);
		employeeHistoryService.createEmployeeHistory(eh);
	}
}
