package app.labexam.serverapp.tcpserver.messageservice;

import app.labexam.serverapp.jpa.entity.EmployeeException;

import javax.ejb.Local;

@Local
public interface IMessageService {

    byte[] processMessage(byte[] message)
            throws EmployeeException;
}
