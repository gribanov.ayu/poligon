package app.labexam.serverapp.tcpserver;

import app.labexam.serverapp.jpa.entity.EmployeeException;
import app.labexam.serverapp.jpa.services.EmployeeHistoryService;
import app.labexam.serverapp.jpa.services.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.ip.tcp.TcpInboundGateway;
import org.springframework.integration.ip.tcp.connection.*;
import org.springframework.messaging.MessageChannel;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Configuration
public class History implements IHistory, ApplicationListener<TcpConnectionEvent> {
    @Value("${tcp.server.port}")
    private int port;
    private static final ConcurrentHashMap<String, TcpConnection> tcpConnections = new ConcurrentHashMap<>();
    public final static Logger log = LoggerFactory.getLogger(History.class);

    private TcpConnection currentConnection;

    private final EmployeeService employeeService;
    private final EmployeeHistoryService employeeHistoryService;

    @Autowired
    public History(EmployeeService employeeService, EmployeeHistoryService employeeHistoryService) {
        this.employeeService = employeeService;
        this.employeeHistoryService = employeeHistoryService;
    }

    @Bean
    public TcpNetServerConnectionFactory serverConnectionFactory() {
        return new TcpNetServerConnectionFactory(port);
    }

    @Bean
    public MessageChannel inboundChannel() {
        return new DirectChannel();
    }

    @Bean
    public TcpInboundGateway inboundGateway(AbstractServerConnectionFactory serverConnectionFactory,
                                            MessageChannel inboundChannel) {
        TcpInboundGateway tcpInboundGateway = new TcpInboundGateway();
        tcpInboundGateway.setConnectionFactory(serverConnectionFactory);
        tcpInboundGateway.setRequestChannel(inboundChannel);
        return tcpInboundGateway;
    }

    @Override
    public void onApplicationEvent(TcpConnectionEvent event) {
        this.currentConnection = (TcpConnection) event.getSource();

        if (event instanceof TcpConnectionOpenEvent) {
            log.info("TCP::Open connection event for ID::{}", event.getConnectionId());
            tcpConnections.put(this.currentConnection.getConnectionId(), this.currentConnection);
        } else if (event instanceof TcpConnectionCloseEvent) {
            log.info("TCP::Close connection event for ID::{}", event.getConnectionId());
            if(this.logout())
                tcpConnections.remove(this.currentConnection.getConnectionId());
        }
    }

    @Override
    public String[] getHistory(String name) throws EmployeeException {
        List<String> historyStrings = this.employeeHistoryService.getEmployeeHistoriesByEmployeeLastname(name)
                .stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.toList());

        if(historyStrings.isEmpty())
            throw new EmployeeException("Can't find employee history by this lastname!");
        else
            return historyStrings.toArray(String[]::new);
    }

    @Override
    public String[] getHistory(int code) throws EmployeeException {
        List<String> historyStrings = this.employeeHistoryService.getEmployeeHistoryByEmployeeId(code)
                .stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.toList());

        if(historyStrings.isEmpty())
            throw new EmployeeException("Can't find employee history by this id!");
        else
            return historyStrings.toArray(String[]::new);
    }

    @Override
    public boolean login(String user, String password) {
        return this.employeeService.validateUser(user, password);
    }

    @Override
    public boolean logout() {
        if(tcpConnections.containsKey(this.currentConnection.getConnectionId())) {
            TcpConnection conn = tcpConnections.get(this.currentConnection.getConnectionId());
            conn.close();
            return true;
        }
        return false;
    }
}
