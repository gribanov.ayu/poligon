package app.labexam.serverapp.tcpserver.messageservice;

import app.labexam.serverapp.tcpserver.IHistory;
import app.labexam.serverapp.jpa.entity.EmployeeException;
import app.labexam.serverapp.tcpserver.util.ProjectConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@Slf4j
public class MessageService implements IMessageService {
    private final IHistory history;

    @Autowired
    public MessageService(IHistory history) {
        this.history = history;
    }

    @Override
    public byte[] processMessage(byte[] message) {
        String result = new String(message);
        log.info("TCP::Received message log: {}", result);

        String[] arr = result.strip().split("\\|");

        switch (arr[0]) {
            case "LOGIN":
                if (this.history.login(arr[1], arr[2])) {
                    return ProjectConstants.RETURN_MESSAGE_IS_SUCCESS.string().getBytes();
                } else {
                    return ProjectConstants.RETURN_MESSAGE_IS_FAILURE.string().getBytes();
                }
            case "EMP_HIS_LASTNAME":
                try {
                    String responseContent = Arrays.toString(this.history.getHistory(arr[1]));

                    if(!responseContent.isEmpty())
                        return responseContent.getBytes();
                    else
                        return ProjectConstants.RETURN_MESSAGE_IS_NONE.string().getBytes();
                } catch (EmployeeException e) {
                    log.info(e.getMessage());
                    return ProjectConstants.RETURN_MESSAGE_IS_NONE.string().getBytes();
                }
            case "EMP_HIS_ID":
                try {
                    if(isNumeric(arr[1])) {
                        String responseContent = Arrays.toString(this.history.getHistory(Integer.parseInt(arr[1])));

                        if(!responseContent.isEmpty())
                            return responseContent.getBytes();
                        else
                            return ProjectConstants.RETURN_MESSAGE_IS_NONE.string().getBytes();
                    }
                } catch (EmployeeException e) {
                    log.info(e.getMessage());
                    return ProjectConstants.RETURN_MESSAGE_IS_NONE.string().getBytes();
                }
            default:
                return ProjectConstants.RETURN_MESSAGE_IS_NONE.string().getBytes();
        }
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null || strNum.equals("") || strNum.contains(".")) {
            return false;
        }

        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }

        return true;
    }
}
