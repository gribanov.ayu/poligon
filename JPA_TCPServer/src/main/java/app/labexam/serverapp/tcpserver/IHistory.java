package app.labexam.serverapp.tcpserver;

import app.labexam.serverapp.jpa.entity.EmployeeException;

import javax.ejb.Local;

@Local
public interface IHistory {
    String[] getHistory(String name) throws EmployeeException;
    String[] getHistory(int code) throws EmployeeException;
    boolean login(String user, String password);
    boolean logout();
}

