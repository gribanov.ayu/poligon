package app.labexam.serverapp.tcpserver.util;

public enum ProjectConstants {
    RETURN_MESSAGE_IS_NONE("NONE"),
    RETURN_MESSAGE_IS_SUCCESS("SUCCESS"),
    RETURN_MESSAGE_IS_FAILURE("FAILURE"),
    ;

    private final String message;
    ProjectConstants(String message) {
        this.message = message;
    }

    public String string() {
        return this.message;
    }
}
