package app.labexam.serverapp.tcpserver.messageservice;

import app.labexam.serverapp.jpa.entity.EmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;

@MessageEndpoint
public class TcpServerEndpoint {
    private final IMessageService messageService;

    @Autowired
    public TcpServerEndpoint(IMessageService messageService) {
        this.messageService = messageService;
    }

    @ServiceActivator(inputChannel = "inboundChannel")
    public byte[] process(byte[] message) throws EmployeeException {
        return this.messageService.processMessage(message);
    }
}
