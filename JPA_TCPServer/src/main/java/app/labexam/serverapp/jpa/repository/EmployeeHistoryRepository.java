package app.labexam.serverapp.jpa.repository;

import app.labexam.serverapp.jpa.entity.EmployeeHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeHistoryRepository extends JpaRepository<EmployeeHistory, Integer> {
    @Query("SELECT p FROM employeehistory p WHERE p.employee.id = :emp_id")
    List<EmployeeHistory> getAllByEmployeeId(@Param("emp_id")Integer emp_id);

    @Query("SELECT p FROM employeehistory p WHERE LOWER(p.employee.last_name) = LOWER(:last_name)")
    List<EmployeeHistory> getEmployeeHistoriesByEmployeeLastname(@Param("last_name")String last_name);
}
