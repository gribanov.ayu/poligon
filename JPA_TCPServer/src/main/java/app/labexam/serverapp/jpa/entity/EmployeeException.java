package app.labexam.serverapp.jpa.entity;

public class EmployeeException extends Exception {
    public EmployeeException(String e) {
        super("MY EMPLOYEE EXCEPTION::" + e);
    }
}
