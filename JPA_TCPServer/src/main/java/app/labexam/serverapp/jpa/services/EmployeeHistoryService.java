package app.labexam.serverapp.jpa.services;

import app.labexam.serverapp.jpa.entity.EmployeeHistory;
import app.labexam.serverapp.jpa.repository.EmployeeHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeHistoryService {
    private final EmployeeHistoryRepository employeeHistoryRepository;

    @Autowired
    public EmployeeHistoryService(EmployeeHistoryRepository employeeHistoryRepository) {
        this.employeeHistoryRepository = employeeHistoryRepository;
    }

    public void createEmployeeHistory(EmployeeHistory employeeHistory) {
        this.employeeHistoryRepository.save(employeeHistory);
    }

    public List<EmployeeHistory> getEmployeeHistoryByEmployeeId(Integer employee_id) {
        return this.employeeHistoryRepository.getAllByEmployeeId(employee_id);
    }

    public List<EmployeeHistory> getEmployeeHistoriesByEmployeeLastname(String last_name) {
        return this.employeeHistoryRepository.getEmployeeHistoriesByEmployeeLastname(last_name);
    }
}
