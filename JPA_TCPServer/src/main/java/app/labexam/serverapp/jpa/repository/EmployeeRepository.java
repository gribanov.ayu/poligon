package app.labexam.serverapp.jpa.repository;

import app.labexam.serverapp.jpa.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("SELECT p FROM employee p WHERE p.login = :login AND p.password = :password")
    Employee findByLoginAndPassword(@Param("login")String login, @Param("password")String password);
}
