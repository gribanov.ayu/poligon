package app.labexam.serverapp.jpa.services;

import app.labexam.serverapp.jpa.entity.Employee;
import app.labexam.serverapp.jpa.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public void createEmployee(Employee employee) {
        this.employeeRepository.save(employee);
    }

    public boolean validateUser(String username, String password) {
        Employee employee = this.employeeRepository.findByLoginAndPassword(username, password);
        return employee != null;
    }
}
