<%@ page language="java"
         contentType="text/html; charset=UTF-8"
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
</head>

<style>
    .bordered {
        width: 300px;
        height: 100px;
        border: 2px solid darkolivegreen;
        padding: 20px;
    }
</style>

<center>
    <h3>Examination Lab :: WEB-TCP_Client / TCP_Server-Database</h3><br>
    <div class="bordered">
        <form action="loginServlet" method="post" name="loginServlet">
            Username: <input type="text" name="user" required/><br><br>
            Password: <input type="password" name="pass" required/><br><br>
            <input type="submit" value="Login!" width="25"><br>
        </form>
    </div>
</center>
</body>
</html>