<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="app.labexam.clientapp.emphisrecord.EmployeeHistoryRecord" %>
<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         session="true"
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registered</title>
</head>
<body>
<style>
    .bordered {
        width: 300px;
        height: 75px;
        padding: 10px;
        border: 2px solid darkolivegreen;
    }
    .bordered2 {
        width: 300px;
        height: 15px;
        padding: 10px;
        border: 2px solid darkolivegreen;
    }
</style>
<center>

    <div class="bordered2">
        <form action="logoutServlet" method="post">
            <input type="submit" value="Logout">
        </form>
    </div>
    <br>
    <div class="bordered">
        <form action="empHisIdServlet" method="post">
            Please enter employee's ID: <input type="text" name="idx"/><br><br>
            <input type="submit" value="Show history"><br>
        </form>
    </div>
    <br>
    <div class="bordered">
        <form action="empHisLastnameServlet" method="post">
            Please input employee's lastname: <input type="text" name="lstName"/><br><br>
            <input type="submit" value="Show history"><br>
        </form>
    </div>

    <%
    String message = (String)session.getAttribute("EmpHis");

    if(message == null) {
        message = "";
    }

    if(!message.isEmpty() && !message.equals("NONE")) {
        List<String> items = new ArrayList<>();

        List<String> lst = Arrays.asList(message.strip()
                .replace("[", "")
                .replace("]", "")
                .split(", "));
        lst.forEach(i -> {
            String[] arr = i.strip().split("_");
            items.add(new EmployeeHistoryRecord(arr[0], arr[1], arr[2], arr[3], arr[4]).toString());
        });

        for(String i: items) {
    %>
    <h3> <%= i %></h3>
    <%
        }
    } else {
    %>
    <h3> <%= message %></h3>
    <%
    }
    %>

</center>
</body>
</html>
