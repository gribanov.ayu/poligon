package app.labexam.clientapp.tcpclient;

import javax.ejb.Local;
import java.io.IOException;

@Local
public interface IClient {
    void logout() throws IOException;
    boolean login(String user, String pass);
    String getEmployeeHistoryByLastName(String lastname);
    String getEmployeeHistoryById(int idx);
}
