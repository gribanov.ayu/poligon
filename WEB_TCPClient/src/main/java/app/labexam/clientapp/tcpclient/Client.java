package app.labexam.clientapp.tcpclient;

import javax.enterprise.context.SessionScoped;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

@SessionScoped
public class Client implements IClient, Serializable {
    private final PrintWriter toServer;
    private final BufferedReader fromServer;
    private final Socket socket;

    public Client() throws IOException {
        socket = new Socket(InetAddress.getByName("localhost"), 801);

        toServer = new PrintWriter(socket.getOutputStream(),true);
        fromServer =
                new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
    }

    public void logout() throws IOException {
        toServer.println("DROP_CONNECTION_SESSION");
        fromServer.readLine();
        socket.close();
    }

    public boolean login(String user, String pass) {
        try {
            toServer.println("LOGIN|" + user + "|" + pass);
            return fromServer.readLine().contains("SUCCESS");
        } catch(IOException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public String getEmployeeHistoryByLastName(String lastname) {
        try {
            toServer.println("EMP_HIS_LASTNAME|" + lastname);
            return fromServer.readLine();
        } catch(IOException ex) {
            ex.printStackTrace();
            return "NONE";
        }
    }

    public String getEmployeeHistoryById(int idx) {
        try {
            toServer.println("EMP_HIS_ID|" + idx);
            return fromServer.readLine();
        } catch(IOException ex) {
            ex.printStackTrace();
            return "NONE";
        }
    }
}