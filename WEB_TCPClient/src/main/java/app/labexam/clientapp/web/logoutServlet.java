package app.labexam.clientapp.web;

import app.labexam.clientapp.tcpclient.IClient;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/logoutServlet", name = "logoutServlet")
public class logoutServlet extends HttpServlet {
    @Inject
    private IClient client;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        client.logout();
        request.getSession(true).invalidate();
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
