package app.labexam.clientapp.web.emphisrecord;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class EmployeeHistoryRecord {
    public String name;
    public String last_name;
    public String position;
    public LocalDate hire_date;
    public LocalDate  dismiss_date;

    public EmployeeHistoryRecord(String name, String last_name, String position, String hire_date, String dismiss_date) {
        this.name = name;
        this.last_name = last_name;
        this.position = position;
        this.hire_date = LocalDate.parse(hire_date);
        this.dismiss_date = LocalDate.parse(dismiss_date);
    }

    @Override
    public String toString() {
        return this.name + " " + this.last_name + " -> " +
                "Position:\t" + this.position + "|" +
                "Hire date:\t" + this.hire_date + "|" +
                "Dismiss date:\t" + this.dismiss_date;
    }
}