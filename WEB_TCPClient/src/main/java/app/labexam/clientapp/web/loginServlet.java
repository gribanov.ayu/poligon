package app.labexam.clientapp.web;

import app.labexam.clientapp.tcpclient.IClient;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(value = "/loginServlet", name = "loginServlet")
public class loginServlet extends HttpServlet {
    @Inject
    private IClient client;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String user = request.getParameter("user");
        String pass = request.getParameter("pass");

        if(!client.login(user, pass)) {
            client.logout();
            request.getSession(true).invalidate();
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/registered.jsp").forward(request, response);
        }
    }
}
