package app.labexam.clientapp.web;

import app.labexam.clientapp.tcpclient.IClient;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/empHisLastnameServlet", name = "empHisLastnameServlet")
public class empHisLastnameServlet extends HttpServlet {
    @Inject
    private IClient client;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String lastname = request.getParameter("lstName");

        if(!lastname.isEmpty()) {
            String result = client.getEmployeeHistoryByLastName(lastname);
            request.getSession(true).setAttribute("EmpHis", result);
        } else {
            request.getSession(true).setAttribute("EmpHis", "");
        }
        request.getRequestDispatcher("registered.jsp").forward(request, response);
    }
}
