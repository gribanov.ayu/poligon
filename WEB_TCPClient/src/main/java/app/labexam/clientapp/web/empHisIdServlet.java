package app.labexam.clientapp.web;

import app.labexam.clientapp.tcpclient.IClient;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/empHisIdServlet", name = "empHisIdServlet")
public class empHisIdServlet extends HttpServlet {
    @Inject
    private IClient client;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String index = request.getParameter("idx");

        if (isStringOnlyDigit(index)) {
            String result = client.getEmployeeHistoryById(Integer.parseInt(index));
            request.getSession(true).setAttribute("EmpHis", result);
        } else {
            request.getSession(true).setAttribute("EmpHis", "NONE");
        }
        request.getRequestDispatcher("registered.jsp").forward(request, response);
    }

    public static boolean isStringOnlyDigit(String str) {
        return !str.equals("") && str.matches("^[0-9]*$");
    }
}
